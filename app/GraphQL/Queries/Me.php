<?php

namespace App\GraphQL\Queries;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

final class Me
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {

        if (!Auth::guard('sanctum')->check()) {
            throw new AuthenticationException('Not authenticated');
        }

        return Auth::guard('sanctum')->user();
    }
}
